[AC Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(output)"} {524291,0,"V(n010)"}
      X: ('K',1,10,0,100000)
      Y[0]: (' ',0,1e-009,20,100)
      Y[1]: (' ',0,-700,70,70)
      Volts: ('m',0,0,0,-0.5,0.1,0.5)
      Log: 1 2 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
